# OS and Linux Intro Class

This class we will look at OS systems and mostly linux.

### OS

OS stands for operating sytem. They are a levle of abstraction on the bare metal.

Some have GUI (graphical user interface) such as andriod, microsoft, macOS and lots of linux distributions 

Others do not.

Some are paid others are open source.

We will be using Linux alot.

### Linux 

There are many Linux distributions, such as:

- Ubuntu (debian) (user friendly)
- Fedor 
- Arc
- RedHad
- Kali (security testing and penetration testing/ethical hacking)

### Bash and shells

Linux comes with bash terminals, and they might have different shells.

Different shells behave slightly differently.

- bash
- oh-my-zsh
- gitbash
- ect

**Small differences in some commands**

#### Package managers 

performed through package manager 

- RHEL = yum, dnf or rpm 
- Debian = apt-get, aptitude or dpkg

These help intall software - such as python or nginx and apache 

#### Aspects

- Everything is a file 
- Everything can interact with everything - great for automation 

#### 3 File Descriptors 

To interact with programs and with files we can use redirect and play around with 0, 1, 2that are stdin, stdout and stderr

- 0 is Standard input (stdin)
- 1 is Standard output (stdout)
- 2 is Standard error (stderr)

### Two Important Hard Paths `/` and `~`

`/` - means root directory

`~` - means `/users/your_login_user/

### Environment variables

Environment is a place where code runs.
In bash we might want to set some variables.

Variable is like a box, you have a name and you add stuff to it. You can open it later.

In bash we define a variable simply by:

``` bash

# setting a variable
$ MY_VAR="<thing>"

# call the variable using echo $
$ echo $<name>

# reassign the variabe 
$ MY_VAR="<newthing>"

# Call other variables the same way
$ echo $<variable>

```

What happens when closing a terminal or opening another one?

Once terminal session is closed, all varibles not in the path will be lost.

#### Child processes

A process that is initiated under another process, like running another bash script.

It goes via the $PATH but is a new "terminal" essentially.

This happens becuause the `$ ./bash_file.sh` runs as a child process - a new terminal 

To make a Variable Available to a child process you need to `$ export`.

#### Path 

Terminal and basg process follow the PATH 

Terminals and bash process follow the PATH

There is a PATH for login users that have a profile and the ones without 
```bash
> .zprofile
> .zshrc

# for bash shell without oh-my-zsh:
> .bashrc
> .bash_profile
> .bashprofile

```

#### Setting and defining 

You need to add them to a file in the path and export them 

### Common Commands

```bash

# Check vairable on terminal 
$ ENV

# Changin oermisions
$ chmod +rwx <filename>

```


### Permisions

### users and sudoers

## Wild Cards
### Matchers 

Use this to help match files or content in files.

```bash
# any number of characters to a side 
$ ls <searchterm>*
$ ls *<searchterm>
$ ls *<searchterm>*

# ? for a specific number of characters
$ ls <search>????

# List of specific characters (each bracket represents a single character)
$ ls <search>[aeiou][a-z][a-zA-Z0-9]

```

### Redirects 

Redirects take the stdout or stderr and can send it to a different location. 

Every command has 3 defaults 

- stdin (0)
- stdout (1)
- stderr (2)

example:
```bash
$ ls
> README.md       example         example.txt     notes
bash_file.sh    example.jpeg    example2        this_is_an_exam
## the list that prints is the stdout!
```

example giving `ls` an stdin
```bash
$ ls example????
> example.txt
## the output is stdout
```

If there is an error, you get a stderr
```bash
$ ls example??
> zsh:no matches found
# the above is a stderr
```

The cool thing is you can redirect this!

##### > and >>

This `>` will redirect and truncate 

this `>>` will redirect and append 

use it with numbers to decide what o redirect.

#### Redirects example 

```bash 
# initially when cat example is run returned will be 
$ cat example
> Hello
> my name 
> is Garth 

# First example will use just >
echo 'Hi Garth' > example
> cat example
> Hi Garth
## The initial text has been replaced

# Second example using >>
echo 'Hi Garth' >> example
cat example
> Hello
> my name 
> is Garth 
> Hi Garth
## The inital text has been appended to

```

### Pipes

piping redirects and makes it stdin of another program 

pipes when places between two programs make the output of one program the input of the other, the symbol to pipe is `$ |`

### GREP
Grep searches the input files for lines containing a match to a given pattern list. When it finds a match in a line, it copies the line to standard output (by default), or whatever other sort of output you have requested with options.

Grep searchs the files supplied of a match to the given search term. When it finds matches it copies them to the standard output or wherever it has been directed to

#### Example of piping and GREP
```bash
# for example when the code below is executed
$ ls -l | grep exam

## for a folder conating the following when $ ls -l is done by itself
## -rw-r--r--  1 garthjackson  staff  3984 13 Jan 17:13 README.md
## -rwxr-xr-x  1 garthjackson  staff   219 13 Jan 16:39 bash_file.sh
## -rw-r--r--  1 garthjackson  staff     0 13 Jan 16:46 example
## -rw-r--r--  1 garthjackson  staff     0 13 Jan 16:46 example.jpeg
## -rw-r--r--  1 garthjackson  staff     0 13 Jan 16:46 example.txt
## -rw-r--r--  1 garthjackson  staff     0 13 Jan 16:46 example2
## -rw-r--r--  1 garthjackson  staff     0 13 Jan 14:05 notes
## -rw-r--r--  1 garthjackson  staff    12 13 Jan 17:04 stdout_file
## -rw-r--r--  1 garthjackson  staff     0 13 Jan 16:47 this_is_an_exam

# the following is returned 
> -rw-r--r--  1 garthjackson  staff     0 13 Jan 16:46 example
> -rw-r--r--  1 garthjackson  staff     0 13 Jan 16:46 example.jpeg
> -rw-r--r--  1 garthjackson  staff     0 13 Jan 16:46 example.txt
> -rw-r--r--  1 garthjackson  staff     0 13 Jan 16:46 example2
> -rw-r--r--  1 garthjackson  staff     0 13 Jan 16:47 this_is_an_exam

```

### Redirecting to stdout, stderr and out
You can redirect outputs 


## As stdout is represented by 1 and stderr is represented by 2 this will redirect the information based on wether teh command succeds or not

```bash
# using the same files as above when 
# When the command suceds
$ ls -l | grep exam 1> stdout 2> stderr
$ cat stdout
> -rw-r--r--  1 garthjackson  staff     0 13 Jan 16:46 example
> -rw-r--r--  1 garthjackson  staff     0 13 Jan 16:46 example.jpeg
> -rw-r--r--  1 garthjackson  staff     0 13 Jan 16:46 example.txt
> -rw-r--r--  1 garthjackson  staff     0 13 Jan 16:46 example2
> -rw-r--r--  1 garthjackson  staff     0 13 Jan 16:47 this_is_an_exam
$ cat stderr

# When the command fails
$ ls -l | grp exam 1> stdout 2> stderr
$ cat stdout
$ cat stderr
> zsh: command not found: grp

# use >> to appdend
# to redirect both err and out to the same place use &
$ ls -l | grp exan &> out
$ cat out 
> zsh: command not found: grp

$ ls -l | grep exam &> out
> -rw-r--r--  1 garthjackson  staff     0 13 Jan 16:46 example
> -rw-r--r--  1 garthjackson  staff     0 13 Jan 16:46 example.jpeg
> -rw-r--r--  1 garthjackson  staff     0 13 Jan 16:46 example.txt
> -rw-r--r--  1 garthjackson  staff     0 13 Jan 16:46 example2
> -rw-r--r--  1 garthjackson  staff     0 13 Jan 16:47 this_is_an_exam

### can also use `ls -l | grep exam >out 2>&1`

```

#### & Sending things to the background

Use `$ &` after the code to send it into the background
```bash
# example 
$ sleep 50 &
```


